"""
    this is a python script that will loop through files stored in a folder and rename them.

    @Author: Frank U.N
    @version: 1.0
    @python version: 3.x

"""

import os
import random
import string 
import time
from datetime import datetime

start_time = datetime.now()

def generate_random_alph(strLen=2):
    #this function will generate random strings
    alph = string.ascii_lowercase
    gen_alph = (random.choice(alph) for i in range(strLen))

    return ''.join(gen_alph)


def getdir():
    #this function gets the path-to-files
    try:
        dirname = input("Enter the path/directory name: ")
        #check if dir or path exists
        assert os.path.exists(dirname)
        path = os.walk(dirname)

        print("\n[*] Initiating the renaming of files in the selected dir: \'{}\'".format(dirname))

    except AssertionError:
        print("[!] The path or directory: \'{}\' entered does not exist".format(dirname)) 
        print("\n[!] Please run the code again and recheck your path or directory name")
        exit(0)

    return path


def rename_f():
    #this function renames the files

    dirn = getdir()
    for dir_, subdirs, files in dirn:
        for file_ in files:
            #rename with two random alphabets
            file_new = file_ +"_"+generate_random_alph()
            #after rename save to same dir
            os.rename(os.path.join(dir_, file_), os.path.join(dir_, file_new))


def main():
    rename_f()

    print('[+] script ran successfully in {}'.format(datetime.now() - start_time))


if __name__ == "__main__":
    main()
