# Rename Files with Python Script


This is a python script that will loop through files in a folder and rename them.

@Author: Frank


@version: 1.0


@python version: 3.x


## Requirements

-- python3



## Setting Up

- [get python3 installed](https://www.python.org/downloads/)

- open your command prompt or terminal and navigate to the folder where the files you want to rename are located.

- download or clone the script to your pc

- run 


`
python rename_file.py
`


- enter path to directory where files are located

- sip a cup of tea. and watch it work magic.



## What's New

- rename2file.py has the following features


	-- allows user input to change extention

	-- allows user to chose string for renaming file
	-- checks the number of files in a directory

