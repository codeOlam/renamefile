"""
    This is a python script that will loop through files in a folder and retrive file extention.

    @Author: Frank U.N
    @version: 2.0
    @python version: 3.x

"""

import os
import time
from datetime import datetime

start_time = datetime.now()


def getdir():
    #this function gets the path-to-files
    try:
        dirname = input("[*] Enter the path/directory name: ")
        #check if dir or path exists
        assert os.path.exists(dirname)
        path = os.walk(dirname)

        print("\n[*] preparing files in the selected directory: \'{}\'".format(dirname))
        time.sleep(1)
        file_count = next(path)[2]
        print("\a[+] found {} files in this directory".format(len(file_count)))

        path = os.walk(dirname)

    except AssertionError:
        print("\n\a[!] The path or directory: \'{}\' entered does not exist".format(dirname)) 
        print("\n[!] Please run the code again and recheck your path or directory name")
        exit(0)

    return path


def rename_f():
    #this function renames the files


    dirn = getdir()
    time.sleep(3)
    ch_ext = input("\n[*] If you wish to change file extention, enter file extention, (e.g; .mp4) or press enter to continue: ")
    rename_str = input("\n[*] Please provide string you want to rename file with or press enter to continue without renaming files: ")

    for dir_, subdirs, files in dirn:
        for file_ in files:
            #rename with two random alphabets
            file__, ext = os.path.splitext(file_)

            if ch_ext !="" and rename_str=="":
                file_new = file__+""+ch_ext
                os.rename(os.path.join(dir_, file_), os.path.join(dir_, file_new))
            elif ch_ext=="" and rename_str!="":
                if ext=="" or ext !="":
                    file_new = file__ +"_"+rename_str+ext
                    #after rename save to same dir
                    os.rename(os.path.join(dir_, file_), os.path.join(dir_, file_new))
            elif ch_ext=="" and rename_str=="":
                print("\n\a[-] you have not selected any operation to be done on the files. \nExiting script!")
                break


def main():
    rename_f()

    print('\n\a[+] script ran successfully in {}'.format(datetime.now() - start_time))


if __name__ == "__main__":
    main()
